<?php

/**
 * Admin setting for uc_cp_labels
 */
function uc_cp_labels_admin_form($form, &$form_state) {

  $form['status'] = array(
    '#type' => 'fieldset',
    '#title' => 'Status',
    '#tree' => FALSE,
  );

  $status = variable_get('uc_cp_labels_status', 'development');
  $form['status']['uc_cp_labels_status'] = array(
    '#type' => 'radios',
    '#title' => 'Mode',
    '#options' => array(
      'development' => 'Development',
      'production' => 'Production',
    ),
    '#default_value' => $status,
    '#description' => t('Register with the <a href="https://www.canadapost.ca/cpotools/apps/drc/home">Canada Post developer program</a> to get development and production API keys (which consist of a username and password). Test label creation using "Development" before creating real labels with "Production".'),
  );

  if ($status == 'development') {
    $form['status']['uc_cp_labels_remove'] = array(
      '#markup' => t('Use this link to delete any shipments created while the site was in "development" mode') . ' <strong>&raquo; <a href="/admin/store/settings/uc_cp_labels_delete">' . t('Delete all test shipment data') . '</strong></a>',
    );
  }

  $form['status']['uc_cp_labels_debug'] = array(
    '#type' => 'radios',
    '#title' => 'Debugging',
    '#options' => array(
      0 => 'Off',
      1 => 'On',
    ),
    '#default_value' => variable_get('uc_cp_labels_debug', 0),
    '#description' => 'Turn this on to log more info about requests and replies.',
  );


  $form['status']['uc_cp_labels_language'] = array(
    '#type' => 'radios',
    '#title' => 'Language',
    '#options' => array(
      'en' => 'English',
      'fr' => 'French',
    ),
    '#default_value' => variable_get('uc_cp_labels_language', 'en'),
  );

  $form['acct'] = array(
    '#type' => 'fieldset',
    '#title' => 'Credentials',
    '#tree' => FALSE,
  );

  $form['acct']['uc_cp_labels_username_development'] = array(
    '#type' => 'textfield',
    '#title' => 'Username - Development',
    '#default_value' => variable_get('uc_cp_labels_username_development', NULL),
  );

  $form['acct']['uc_cp_labels_password_development'] = array(
    '#type' => 'textfield',
    '#title' => 'Password - Development',
    '#default_value' => variable_get('uc_cp_labels_password_development', NULL),
  );

  $form['acct']['uc_cp_labels_username_production'] = array(
    '#type' => 'textfield',
    '#title' => 'Username - Production',
    '#default_value' => variable_get('uc_cp_labels_username_production', NULL),
  );

  $form['acct']['uc_cp_labels_password_production'] = array(
    '#type' => 'textfield',
    '#title' => 'Password - Production',
    '#default_value' => variable_get('uc_cp_labels_password_production', NULL),
  );

  $form['acct']['uc_cp_labels_mailedby'] = array(
    '#type' => 'textfield',
    '#title' => 'Canada Post Customer Number',
    '#default_value' => variable_get('uc_cp_labels_mailedby', NULL),
  );

  $form['contract'] = array(
    '#type' => 'fieldset',
    '#title' => 'Contract shipping',
    '#description' => 'Supply these details if you have a parcel agreement with Canada Post, and thus use "contract shipping". This should not be necessary for most Venture One business customers.',
    '#tree' => FALSE,
  );
  $form['contract']['uc_cp_labels_contract'] = array(
    '#type' => 'textfield',
    '#title' => 'Contract/Agreement Number',
    '#default_value' => variable_get('uc_cp_labels_contract', NULL),
  );

  $form['contract']['uc_cp_labels_contract_pay'] = array(
    '#type' => 'select',
    '#title' => 'Contract Shipping Payment Method',
    '#options' => array(
      'Account' => 'Account',
      'CreditCard' => "Credit card",
    ),
    '#default_value' => variable_get('uc_cp_labels_contract_pay', 'Account'),
  );


  $form['contact'] = array(
    '#type' => 'fieldset',
    '#title' => 'Contact info',
    '#tree' => FALSE,
  );
  $form['contact']['uc_cp_labels_store_name'] = array(
    '#type' => 'textfield',
    '#title' => 'Store Name (for return address)',
    '#default_value' => variable_get('uc_cp_labels_store_name', NULL),
  );

  $form['contact']['uc_cp_labels_email'] = array(
    '#type' => 'textfield',
    '#title' => 'Email for delivery notification',
    '#default_value' => variable_get('uc_cp_labels_email', NULL),
  );
  
  
  // Default shipping types
  $form['ship_types'] = array(
    '#type' => 'fieldset',
    '#title' => 'Default shipping types',
    '#description' => 'Set the default CP shipping method to apply to each Ubercart shipping type',
    '#tree' => FALSE,
  );
  $cp_types = array(
    'DOM.EP' => t('Expedited Parcel (Canada)'),
    'DOM.XP' => t('Xpresspost (Canada)'),
    'DOM.PC' => t('Priority (Canada)'),
  );
  $shipping_types = uc_quote_methods();
  foreach ($shipping_types as $type_id => $type) {
    $form['ship_types']['uc_cp_labels_default_' . $type_id] = array(
      '#type' => 'select',
      '#title' => t($type['title']),
      '#options' => $cp_types,
      '#default_value' => variable_get('uc_cp_labels_default_' . $type_id, 'DOM.EP'),
    );
  }
  
  // Node type renaming
  $form['customs'] = array(
    '#type' => 'fieldset',
    '#title' => 'Customs Declaration',
    '#description' => 'If you ship to the USA, the customs declaration requires a list of the items in the order. Choose the source of that description here.',
    '#tree' => FALSE,
  );
  
  $form['customs']['uc_cp_labels_customs'] = array(
    '#type' => 'select',
    '#title' => t('Line item description source'),
    '#options' => array('title' => t('Product name'), 'type' => t('Node type')),
    '#default_value' => variable_get('uc_cp_labels_customs', 'type'),
  );
  
  $form['customs']['types_intro'] = array(
    '#markup' => '<strong>' . t('If you want to customize the descriptive terms for UC node types, you may do so here.') . '</strong>',
  );
  
  $product_types = uc_product_types();
  foreach($product_types as $type) {
    $form['customs']['uc_cp_labels_rename_' . $type] = array(
      '#type' => 'textfield',
      '#title' => $type,
      '#default_value' => variable_get('uc_cp_labels_rename_' . $type, NULL),
    );
  }
  
  return system_settings_form($form);
}


/**
 * admin fn to delete test data
 */
function uc_cp_labels_delete_records() {
  $c = 0;
  $sql = "SELECT order_id FROM {uc_orders} WHERE data LIKE '%uc_cp_labels%'";
  $result = db_query($sql);
  foreach ($result as $row) {
    $order   = uc_order_load($row->order_id);
    $changed = FALSE;
    foreach ($order->data['uc_cp_labels'] as $ship_id => $ship_data) {
      if ($ship_data['uc_cp_status'] == 'development') {
        unset($order->data['uc_cp_labels'][$ship_id]);
        $changed = TRUE;
        $c++;
      }
    }
    if ($changed) {
      uc_order_save($order);
    }
  }

  $dsm = ($c > 0) ? "$c test label(s) deleted." : 'There were no test records to delete.';
  drupal_set_message($dsm);
  drupal_goto('admin/store/settings/uc_cp_labels');
}
